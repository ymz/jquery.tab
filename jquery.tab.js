(function($) {
	$.fn.tab = function(options) {
		var tab, item, style, event, play, time, t;
		var defaults = { 
			// item : '#content .item', //内容选择器 必填项
            style : 'curr', //当前项class 注意不带.
            event : 'mouseover', //触发事件
            play : true, //自动播放开关
            time : 3000 //自动播放定时
		};
		var opts = jQuery.extend(defaults,options);  
		typeof options == "string" && (opts.item = options); 
		tab = this;
		item = $(opts.item);
		style = opts.style;
		event = opts.event;
		play = opts.play;
		time = opts.time;
		init();

		function init(obj) {
			// 绑定事件
			tab.bind(event, function () {
				clearInterval(t);
				change($(this));
			});	

			// 如果设置自动播放，增加定时器
			if (play) {
				tab.bind('mouseout', start);
				item.bind('mouseenter', function() {
					clearInterval(t);
				});
				item.bind('mouseout', start);
				start();
			};
		}

		function start() {
			clearInterval(t);
			t = setInterval(function(){
				change();
			},time);
		}
		function change(curr) {
			if (!curr) {
				// 未指定当前
				curr = tab.filter('.'+style).next().length == 0  ? tab.eq(0) : tab.filter('.'+style).next();
			}
			var index = curr.index();
			tab.removeClass(style);
			curr.addClass(style);
			item.removeClass(style);	
			item.eq(index).addClass(style);
			
		}
	};
})(jQuery);